package config

import (
	"aliyundns/define"
	"errors"
	"fmt"
	"gopkg.in/yaml.v3"
	"os"
)

type Config define.Config

func (a *Config) GetConfig() (cfg *define.Config, err error) {
	file, err := os.Open(define.CfgPath)
	if err != nil {
		return nil, errors.New(fmt.Sprintf("打开配置文件失败: %v", err))
	}
	defer file.Close()
	if yaml.NewDecoder(file).Decode(&cfg) != nil {
		return nil, errors.New(fmt.Sprintf("解析配置文件失败: %v", err))
	}
	return
}
