package loger

import (
	"aliyundns/config"
	"fmt"
	"github.com/phachon/go-logger"
)

var Logger *go_logger.Logger

func OutConsole(msg string) {
	console := &go_logger.ConsoleConfig{
		Color:      true,
		JsonFormat: false,
		Format:     "",
	}
	err := Logger.Attach("console", go_logger.LOGGER_LEVEL_DEBUG, console)
	if err != nil {
		Logger.Infof("%v", err)
	}
	Logger.Errorf("%v", msg)
}

func CreateLogFile() {
	c := config.Config{}
	cfg, err := c.GetConfig()
	if err != nil {
		OutToFile(fmt.Sprintf("读取配置文件错误：%v\n", err))
		return
	}
	fileConfig := &go_logger.FileConfig{
		Filename: cfg.Log.Path,
		LevelFileName: map[int]string{
			Logger.LoggerLevel(cfg.Log.Level): cfg.Log.Path + cfg.Log.Level + ".log",
		},
		MaxSize:    1024 * 1024,
		MaxLine:    10000,
		DateSlice:  "d",
		JsonFormat: false,
		Format:     "",
	}
	err = Logger.Attach("file", go_logger.LOGGER_LEVEL_DEBUG, fileConfig)
	if err != nil {
		Logger.Debugf("%v", err)
	}
}

func OutToFile(msg string) {
	Logger.Debugf("%v", msg)
}
