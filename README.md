# aliyundns

#### 介绍
因有需要对动态IP网络下的服务器做阿里云映射，经常去页面里面改显得很麻烦。有没有一种程序能够自动检测IP变化自动更新域名解析服务的程序，于是在网上也找了一些，结果未成功，可能是程序太久未更新的缘故。因此亲自研究了一下阿里云的接口，自己手撸了一个自动IP变化更新域名解析的程序，程序写的乱七八糟好在能用，后续在完善吧。

#### 安装教程命令如下

1.  git clone https://gitee.com/zhuxiufeng/aliyundns.git
2.  cd aliyundns
3.  go mod tidy

#### 使用说明

1.  使用前对 `config` 目录下的 `config.yaml` 配置文件做相应配置,后修改``define/define.go`` 文件中的 `CfgPath` 变量指向的配置文件路径
2.  配置完成后使用 `go build -o xxx.exe` 进行编译
3.  编译后使用 `sc create [服务名] binPath= '绝对路径\xxx.exe' start= auto displayName= [服务显示名]` 命令将编译好的文件制作为系统服务
4.  启动服务：`net start 服务名`
5.  停止服务：`net stop 服务名`

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


