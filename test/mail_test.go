package test

import (
	"aliyundns/config"
	"aliyundns/utils"
	"fmt"
	"testing"
)

func TestMail(t *testing.T) {
	c := config.Config{}
	cfg, err := c.GetConfig()
	if err != nil {
		fmt.Println("get config error: ", err)
	}
	message := "<h1>IP was modified from<h1>" + "\n" + "12.168.15.5" + "\n" + "<h1>to<h1>" + "\n" + "152.123.125.253"

	err = utils.SendMail(cfg.Mail.SmtpServer, cfg.Mail.FromMail, cfg.Mail.FromPassword, cfg.Mail.ToMail, cfg.Mail.Subject, message, cfg.Mail.Port, cfg.Mail.InsecureSkipVerify)
	if err != nil {
		fmt.Println(err)
	}
}
