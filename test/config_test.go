package test

import (
	"aliyundns/config"
	"fmt"
	"testing"
)

func TestCfg(t *testing.T) {
	c := config.Config{}
	cfg, err := c.GetConfig()
	if err != nil {
		fmt.Println("get config error: ", err)
	}
	fmt.Println(cfg)
}
