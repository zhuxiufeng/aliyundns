package utils

import (
	"crypto/tls"
	"errors"
	"fmt"
	"gopkg.in/gomail.v2"
	//"net/smtp"
)

var mail *gomail.Message

func init() {
	mail = gomail.NewMessage()
}

func SendMail(smtpServer, fromMail, passwd, toMail, subject, msg string, smtpPort int, ssl bool) (err error) {
	mail.SetHeader("From", fromMail)
	mail.SetHeader("To", toMail)
	mail.SetHeader("Subject", subject)
	mail.SetBody("text/html", msg)

	nd := gomail.NewDialer(smtpServer, smtpPort, fromMail, passwd)
	if ssl {
		nd.TLSConfig = &tls.Config{InsecureSkipVerify: ssl}
	}
	if nd.DialAndSend(mail) != nil {
		return errors.New(fmt.Sprintf("send mail failed:%v", err))
	}
	return
}
