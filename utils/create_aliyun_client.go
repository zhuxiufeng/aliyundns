package utils

import "github.com/aliyun/alibaba-cloud-sdk-go/services/alidns"

func CreateAliyunClient(regionID, accessKeyID, accessKeySecret string) (client *alidns.Client, err error) {
	return alidns.NewClientWithAccessKey(regionID, accessKeyID, accessKeySecret)
}
