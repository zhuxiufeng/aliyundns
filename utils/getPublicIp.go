package utils

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func GetPubLicIp(url string) (ip string, err error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", errors.New(fmt.Sprintf("请求 URL 获取公共IP错误: %v", err))
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", errors.New(fmt.Sprintf("读取 Body 数据错误: %v", err))
	}
	return strings.TrimSpace(string(body)), nil
}
