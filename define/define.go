package define

const (
	CfgPath = "C:\\code\\src\\aliyundns\\config\\config.yaml"
)

// Config 配置文件解析结构
type Config struct {
	Ali  *Aliyun `yaml:"aliyun"`
	Log  *Log    `yaml:"log"`
	Mail *Mail   `yaml:"mail"`
}

type Aliyun struct {
	Sleep           int      `yaml:"sleep"`
	AccessKeyId     string   `yaml:"access_key_id"`
	AccessKeySecret string   `yaml:"access_key_secret"`
	RegionId        string   `yaml:"region_id"`
	DomainName      string   `yaml:"domain_name"`
	RecordType      string   `yaml:"record_type"`
	RecordName      []string `yaml:"record_name"`
	Url             string   `yaml:"url"`
}

type Log struct {
	Path    string `yaml:"path"`
	Level   string `yaml:"level"`
	Develop bool   `yaml:"develop"`
}

type Mail struct {
	SmtpServer         string `yaml:"smtp_server"`
	Port               int    `yaml:"port"`
	FromMail           string `yaml:"from_mail"`
	FromPassword       string `yaml:"from_password"`
	ToMail             string `yaml:"to_mail"`
	Subject            string `yaml:"subject"`
	InsecureSkipVerify bool   `yaml:"insecure_skip_verify"`
}
